package noobbot.positions;

import noobbot.car.CarId;

public class CarPosition {

	private CarId id;
	private Double angle;
	private PiecePosition piecePosition;
	
	
	public CarId getId() {
		return id;
	}
	public void setId(CarId id) {
		this.id = id;
	}
	public Double getAngle() {
		return angle;
	}
	public void setAngle(Double angle) {
		this.angle = angle;
	}
	public PiecePosition getPiecePosition() {
		return piecePosition;
	}
	public void setPiecePosition(PiecePosition piecePosition) {
		this.piecePosition = piecePosition;
	}

}

/*
{"msgType": "carPositions", "data": [
{
  "id": {
    "name": "Schumacher",
    "color": "red"
  },
  "angle": 0.0,
  "piecePosition": {
    "pieceIndex": 0,
    "inPieceDistance": 0.0,
    "lane": {
      "startLaneIndex": 0,
      "endLaneIndex": 0
    },
    "lap": 0
  }
},
{
  "id": {
    "name": "Rosberg",
    "color": "blue"
  },
  "angle": 45.0,
  "piecePosition": {
    "pieceIndex": 0,
    "inPieceDistance": 20.0,
    "lane": {
      "startLaneIndex": 1,
      "endLaneIndex": 1
    },
    "lap": 0
  }
}
], "gameId": "OIUHGERJWEOI", "gameTick": 0}
*/