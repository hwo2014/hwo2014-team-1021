package noobbot.positions;

public class Lane {
	private int startLaneIndex;
	private int endLaneIndex;
	
	public int getStartLaneIndex() {
		return startLaneIndex;
	}
	public void setStartLaneIndex(int startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}
	public int getEndLaneIndex() {
		return endLaneIndex;
	}
	public void setEndLaneIndex(int endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}
}