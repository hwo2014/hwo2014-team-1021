package noobbot.positions;

public class PiecePosition {
	private Integer pieceIndex;
	private Double inPieceDistance;
	private Lane lane;
	private Integer lap;
	
	public int getPieceIndex() {
		return pieceIndex;
	}
	public void setPieceIndex(Integer pieceIndex) {
		this.pieceIndex = pieceIndex;
	}
	public Double getInPieceDistance() {
		return inPieceDistance;
	}
	public void setInPieceDistance(Double inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}
	public Lane getLane() {
		return lane;
	}
	public void setLane(Lane lane) {
		this.lane = lane;
	}
	public Integer getLap() {
		return lap;
	}
	public void setLap(Integer lap) {
		this.lap = lap;
	}
	
}