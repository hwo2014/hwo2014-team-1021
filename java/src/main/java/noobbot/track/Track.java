package noobbot.track;

import java.util.List;

public class Track {
	
	private String id;
	private String name;
	private List<TrackPiece> pieces;
	private List<TrackLane> lanes;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<TrackPiece> getPieces() {
		return pieces;
	}
	public void setPieces(List<TrackPiece> pieces) {
		this.pieces = pieces;
	}
	public List<TrackLane> getLanes() {
		return lanes;
	}
	public void setLanes(List<TrackLane> lanes) {
		this.lanes = lanes;
	}
	@Override
	public String toString(){
		return this.name;
	}
	

}
