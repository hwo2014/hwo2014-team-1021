package noobbot.track;

import noobbot.Position;

public class TrackStartingPoint {
	
	private Position position;
	private Double angle;

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Double getAngle() {
		return angle;
	}

	public void setAngle(Double angle) {
		this.angle = angle;
	}

}
