package noobbot.track;

public class TrackPiece {
	
	private Double length;
	private Integer radius;
	private Double angle;
	private boolean switchable;
	private boolean bridge;
	public Double getLenght() {
		return length;
	}
	public void setLenght(Double lenght) {
		this.length = lenght;
	}
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	public Double getAngle() {
		return angle;
	}
	public void setAngle(Double angle) {
		this.angle = angle;
	}
	public boolean isSwitchable() {
		return switchable;
	}
	public void setSwitchable(boolean switchable) {
		this.switchable = switchable;
	}
	
	public void printAll(){
		if(length!=null)
		System.out.println("len:"+length);
	}
	public boolean isBridge() {
		return bridge;
	}
	public void setBridge(boolean bridge) {
		this.bridge = bridge;
	}

}
