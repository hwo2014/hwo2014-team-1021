package noobbot.track;

public class TrackLane {
	
	private Short index;
	private Short distanceFromCenter;
	
	public Short getIndex() {
		return index;
	}
	public void setIndex(Short index) {
		this.index = index;
	}
	public Short getDistanceFromCenter() {
		return distanceFromCenter;
	}
	public void setDistanceFromCenter(Short distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}
	
	
	

}
