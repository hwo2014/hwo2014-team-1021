package noobbot;

import java.util.List;

import noobbot.car.Car;
import noobbot.track.Track;

public class Race {

	private Track track;
	private List<Car> cars;
	private RaceSession raceSession;
	public Track getTrack() {
		return track;
	}
	public void setTrack(Track track) {
		this.track = track;
	}
	public List<Car> getCars() {
		return cars;
	}
	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	public RaceSession getRaceSession() {
		return raceSession;
	}
	public void setRaceSession(RaceSession raceSession) {
		this.raceSession = raceSession;
	}
	@Override
	public String toString(){
		return track.toString();
	}	
}
