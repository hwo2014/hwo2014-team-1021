package noobbot;

public class RaceSession {
	
    private Short laps;
    private Long maxLapTimeMs;
    private boolean quickRace;
	public Short getLaps() {
		return laps;
	}
	public void setLaps(Short laps) {
		this.laps = laps;
	}
	public Long getMaxLapTimeMs() {
		return maxLapTimeMs;
	}
	public void setMaxLapTimeMs(Long maxLapTimeMs) {
		this.maxLapTimeMs = maxLapTimeMs;
	}
	public boolean isQuickRace() {
		return quickRace;
	}
	public void setQuickRace(boolean quickRace) {
		this.quickRace = quickRace;
	}
    
    

}
