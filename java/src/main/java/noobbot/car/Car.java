package noobbot.car;

public class Car {
	
	private CarId id;
	private CarDimensions dimensions;
	private double angle;
	private int lane;
	private int piece;
	private double in_piece_distance;
	private int lap;
	
	public CarId getId() {
		return id;
	}
	public void setId(CarId id) {
		this.id = id;
	}
	public CarDimensions getDimensions() {
		return dimensions;
	}
	public void setDimensions(CarDimensions dimensions) {
		this.dimensions = dimensions;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	public int getPiece() {
		return piece;
	}
	public void setPiece(int piece) {
		this.piece = piece;
	}
	public double getIn_piece_distance() {
		return in_piece_distance;
	}
	public void setIn_piece_distance(double in_piece_distance) {
		this.in_piece_distance = in_piece_distance;
	}
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap = lap;
	}
	
}
