package noobbot.car;

public class CarDimensions {
	
    private Double length;
    private Double width;
    private Double guideFlagPosition;
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = length;
	}
	public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	public Double getGuideFlagPosition() {
		return guideFlagPosition;
	}
	public void setGuideFlagPosition(Double guideFlagPosition) {
		this.guideFlagPosition = guideFlagPosition;
	}
    
    

}
