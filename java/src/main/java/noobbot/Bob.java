package noobbot;

import java.util.ArrayList;
import java.util.List;

import noobbot.car.Car;
import noobbot.car.CarId;
import noobbot.positions.CarPosition;
import noobbot.track.TrackPiece;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Bob {
	
	Gson gson = new Gson();
	JsonParser parsah = new JsonParser();
	Race race;
	InitData initData;	
	Car myCar;
	boolean laneSwitched;
	List<List<Car>> carsByLane = new ArrayList<List<Car>>();
	
	class InitData{
		Race race;
	};
	
	public void setCarsByLanes(MsgWrapper msgFromServer) {
		
		List<Car> cars = race.getCars();
		for (Car car : cars) {
			int lane = car.getLane();
		}
	}
	
	public double getThrottle(MsgWrapper dataFromServer) {
		//System.out.println("Calculating throttle");
		class CarPositions{
			String msgType;
			List<CarPosition> data;
		};
		
		//JsonElement element = parsah.parse(line);
		//JsonArray newPos = element.getAsJsonArray();
		CarPosition[] positions = gson.fromJson(dataFromServer.data.toString(), CarPosition[].class);
		//CarPositions positions = null;
		//positions = gson.fromJson(line, CarPositions.class);
		CarPosition myNewPosition = null;
		for(CarPosition position : positions){
			if(position.getId().getColor().equals(myCar.getId().getColor())){
				myNewPosition = position;
				break;
			}
			
		}
		TrackPiece nextPiece = null;
		TrackPiece thisPiece = race.getTrack().getPieces().get(myNewPosition.getPiecePosition().getPieceIndex());
		if(myNewPosition.getPiecePosition().getPieceIndex() == race.getTrack().getPieces().size()-1){
			nextPiece = race.getTrack().getPieces().get(0);
		}else{
			
			nextPiece = race.getTrack().getPieces().get(myNewPosition.getPiecePosition().getPieceIndex()+1);
		}
		if(thisPiece.getAngle() == null && nextPiece.getAngle()!=null ){
			if( (myNewPosition.getPiecePosition().getInPieceDistance())/
				thisPiece.getLenght() > 0.5){
				System.out.println("Slowing down at piece: "+myNewPosition.getPiecePosition().getPieceIndex()+" With distance: "+ myNewPosition.getPiecePosition().getInPieceDistance());
				return 0.55;
			}
			else return 0.65;
		}else if(thisPiece.getLenght() != null && nextPiece.getAngle() == null){
			return 1;
		}else if(thisPiece.getAngle() != null && nextPiece.getLenght()!=null && myNewPosition.getPiecePosition().getInPieceDistance() > 75){
			System.out.println("Accelerating at piece: "+myNewPosition.getPiecePosition().getPieceIndex()+" With distance: "+ myNewPosition.getPiecePosition().getInPieceDistance());
			return 0.6;
		}else if(thisPiece.getAngle()!=null && nextPiece.getAngle()!= null){
			return 0.55;
		}else if(thisPiece.getLenght()!=null &&nextPiece.getLenght()!=null){
			return 1;
		}else{
			return 0.5;
		}		
	}
	
	public void initCar(MsgWrapper msgFromServer) {
		JsonElement element = parsah.parse(msgFromServer.data.toString());
		myCar = new Car();
		myCar.setId(gson.fromJson(element, CarId.class));
		int a;
	}
	
	public void init(MsgWrapper msgFromServer){
		JsonElement element = parsah.parse(msgFromServer.data.toString());	
		initData = gson.fromJson(element, InitData.class);
		race = initData.race;
	
		List<Car> cars = race.getCars();
		for (Car car : cars) {
			if(car.getId().getColor().equals(myCar.getId().getColor())) {
				myCar = car;
			}
		}

	}
}
